//
//  SectionTableViewController.swift
//  FoodDrawer
//
//  Created by Shahab Ejaz on 08/09/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

import UIKit

class SectionTableViewController: UITableViewController {

    var data = [Dictionary<String,AnyObject>]()
    var selectedIndexes = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.contentInset = UIEdgeInsets(top: 90, left: 0, bottom: 40, right: 0)
        if let path = Bundle.main.path(forResource: "FoodDrawerData", ofType: ".plist") {
            let dict = NSDictionary(contentsOfFile: path)
            self.data = dict!.value(forKey: "Sections") as! Array
        }
        if let arr = UserDefaults.standard.array(forKey: "selectedSections") as? [Int] {
            self.selectedIndexes = arr
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.data.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.textLabel?.text = self.data[indexPath.row]["SectionTitle"] as? String
        if self.selectedIndexes.contains(indexPath.row) {
            cell.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedIndexes.contains(indexPath.row) {
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .none
            self.selectedIndexes.remove(at: self.selectedIndexes.index(of: indexPath.row)!)
        } else {
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .checkmark
            self.selectedIndexes.append(indexPath.row)
            self.selectedIndexes.sort(by:<)
        }
    }

}
