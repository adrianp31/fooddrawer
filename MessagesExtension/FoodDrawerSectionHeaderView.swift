//
//  FoodDrawerSectionHeaderView.swift
//  FoodDrawer
//
//  Created by Shahab Ejaz on 05/09/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

import UIKit

class FoodDrawerSectionHeaderView: UICollectionReusableView {
    @IBOutlet var sectionTitle: UILabel!
    func configure(usingTitle title:String) {
        self.sectionTitle.text = title
    }
}
